import { Injectable } from '@angular/core';
import Category from '../interfaces/category.interface';

@Injectable({providedIn: 'root'})
export default class CategoryService {
  public rimkaCategories: Category[] = [
      {id: '1', icon: '/assets/icon/icon-heart.svg', title: 'Obľúbené recepty'},
      {id: '-1', icon: '/assets/icon/icon-all-recipes.svg', title: 'Všetky recepty'}, // special kind of category
      {id: 'cheesecaky-pecene', icon: '', title: 'Cheesecaky', subtitle: 'pečené'},
      {id: 'cheesecaky-nepecene', icon: '', title: 'Cheesecaky', subtitle: 'nepečené'},
      {id: 'dezerty-pecene', icon: '', title: 'Dezerty', subtitle: 'pečené'},
      {id: 'dezerty-nepecene', icon: '', title: 'Dezerty', subtitle: 'nepečené'},
      {id: 'slanotky', icon: '', title: 'Slanotky'},
      {id: 'vianocne-recepty', icon: '', title: 'Vianočné recepty'},
    ];
  public chlopCategories: Category[] = [
    {id: '1', icon: '/assets/icon/icon-heart.svg', title: 'Obľúbené recepty'},
    {id: '-1', icon: '/assets/icon/icon-all-recipes.svg', title: 'Všetky recepty'},
    {id: 'natierky-na-chlebicok', icon: '', title: 'Nátierky na chľebičok'},
    {id: 'slane-na-prekusene', icon: '', title: 'Slané na prekušene'},
    {id: 'coska-sladke', icon: '', title: 'Coška sladké'},
    {id: 'masko-tak-i-onak', icon: '', title: 'Mäsko tak i onak'},
    {id: 'sicke-mozne-cestoviny', icon: '', title: 'Šicke možné cestoviny'},
    {id: 'i-take-daco-bez-masa', icon: '', title: 'I také daco bez mäsa'},
    {id: 'zeby-vam-nesedlo-na-zaludok', icon: '', title: 'Žeby vám nešedlo na žalúdok'},
  ];

  private _selectedCategory: Category;
  public set selectedCategory(category: Category) {
    // when "All categories" is selected, basically that means no category is selected (no filter)
    if (category && category.id === '-1') {
      this._selectedCategory = null;
    } else {
      this._selectedCategory = category;
    }
  }

  public get selectedCategory() {
    return this._selectedCategory;
  }
}
