export default interface Category {
  id: string; // it's "slug" in Ghost admin
  icon?: string;
  title: string; // this part of category title will be bold
  subtitle?: string; // this part will be not bold
}
