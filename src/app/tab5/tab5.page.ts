import { Component } from '@angular/core';
import CacheService from '../services/cache.service';
import PopupsService from '../services/popups.service';
import { Plugins } from '@capacitor/core';
import * as Sentry from '@sentry/angular';

const { App } = Plugins;
@Component({
  selector: 'app-tab5',
  templateUrl: 'tab5.page.html',
  styleUrls: ['tab5.page.scss']
})
export class Tab5Page {

  constructor(private cacheService: CacheService, private popupsService: PopupsService) {}

  public openFb() {
    location.href = 'https://www.facebook.com/rimkaklaudi';
  }

  public openInsta() {
    location.href = 'https://www.instagram.com/klaudi_rimka';
  }

  public async clearCache() {
    await this.cacheService.clearAll();
    await this.popupsService.showAlert('Hotovo!', '', 'Dočasné údaje v pamäti cache boli vymazané. Ak sa aplikácia zavrie, môžete ju opäť otvoriť manuálne.');
    Sentry.captureMessage('Clear cache fired manually');

    App.exitApp();
  }
}
