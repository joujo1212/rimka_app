/**
 * Simplified version of ghost's Tag with only id (it's 'slug' in ghost) and name
 */
export default interface TagAsCategory {
  id: string;
  name: string;
}
