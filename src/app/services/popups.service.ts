import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';

@Injectable({providedIn: 'root'})
export default class PopupsService {

  constructor(private toastController: ToastController, private alertController: AlertController,
              private loadingController: LoadingController) {
  }

  /**
   *
   * @param text
   * @param duration Duration in millis. If it's null/0/undefined, toast will be shown never ending. Default 2seconds
   * @param position Position of toast, Optional, default is 'top'
   */
  public async showToast(text: string, duration = 2000, position: 'top' | 'bottom' | 'middle' = 'top'): Promise<HTMLIonToastElement> {
    const toast = await this.toastController.create({
      message: text,
      duration: duration ? duration : undefined,
      position
    });
    toast.present();
    return toast;
  }

  public async showAlert(header: string, subHeader: string, message: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header,
      subHeader,
      message,
      buttons: ['OK']
    });

    await alert.present();
  }


  public async showLoad(text: string, showSpinner?: boolean, duration?: number): Promise<HTMLIonLoadingElement> {
    const loading = await this.loadingController.create({
      spinner: showSpinner ? 'circles' : null,
      duration: duration ? duration : undefined,
      message: text,
      translucent: true,
      cssClass: 'custom-class custom-loading',
      backdropDismiss: false
    });
    loading.present();
    return loading;
  }
}
