import { Injectable } from '@angular/core';
import GhostContentAPI, { GhostAPI } from '@tryghost/content-api';

@Injectable({providedIn: 'root'})
export default class ApiService {
  private apiInstance: GhostAPI;

  private initGhostApi() {
    this.apiInstance = new GhostContentAPI({
      url: 'https://blog.invelogy.sk',
      key: '178270febfde3c4947585edc06',
      version: 'v3'
    });
  }
  public getApi(): GhostAPI {
    if (!this.apiInstance) {
      this.initGhostApi();
    }
    return this.apiInstance;
  }
}
