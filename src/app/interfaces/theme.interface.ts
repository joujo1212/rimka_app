export interface Theme {
  name: string;
  properties: any;
}
// filters generated here: https://dev.to/jsm91/css-filter-generator-to-convert-from-black-to-target-hex-color-188h
export const rimkaTheme: Theme = {
  name: "rimkaTheme",
  properties: {
    "--primary-color": "#f5a0c5",
    "--accent-color": "#BB6990",
    "--header-bg-color": "rgba(255, 240, 247, 0.9)",
    "--header-secondary-bg-color": "rgba(167, 208, 219, 0.9)",
    "--shadow-colored": "rgba(187, 105, 144, 0.3)", //#BB6990
    "--shadow-colored-secondary": "rgba(129,183,196,0.3)", //#81B7C4
    "--text-color": "#442F2F",
    "--light-text-color": "#fff",
    "--secondary-color": "#81B7C4",
    "--secondary-light-color": "#A7D0DB",
    "--filter-primary-color": "invert(89%) sepia(34%) saturate(2078%) hue-rotate(287deg) brightness(107%) contrast(92%)",
    "--filter-accent-color": "invert(53%) sepia(10%) saturate(2133%) hue-rotate(279deg) brightness(91%) contrast(78%)",
    "--filter-text-color": "invert(20%) sepia(13%) saturate(942%) hue-rotate(314deg) brightness(94%) contrast(95%)",
    "--filter-secondary-color": "invert(72%) sepia(10%) saturate(1011%) hue-rotate(144deg) brightness(94%) contrast(91%)"
  }
};

export const chlopTheme: Theme = {
  name: "chlopTheme",
  properties: {
    // "--primary-color": "#cbcdca",
    "--primary-color": "#646F60",
    "--accent-color": "#3e453d",
    "--header-bg-color": "rgba(211,217,210,0.9)",
    "--header-secondary-bg-color": "rgba(167, 208, 219, 0.9)",
    "--shadow-colored": "rgba(62,69,61,0.3)", //#3e453d
    "--shadow-colored-secondary": "rgba(129,183,196,0.3)", //#81B7C4
    "--text-color": "#442F2F",
    "--light-text-color": "#fff",
    "--secondary-color": "#81B7C4",
    "--secondary-light-color": "#A7D0DB",
    "--filter-primary-color": "invert(44%) sepia(6%) saturate(865%) hue-rotate(59deg) brightness(91%) contrast(83%)",
    "--filter-accent-color": "invert(24%) sepia(5%) saturate(897%) hue-rotate(66deg) brightness(94%) contrast(88%)",
    "--filter-text-color": "invert(20%) sepia(13%) saturate(942%) hue-rotate(314deg) brightness(94%) contrast(95%)",
    "--filter-secondary-color": "invert(72%) sepia(10%) saturate(1011%) hue-rotate(144deg) brightness(94%) contrast(91%)"
  }
};
