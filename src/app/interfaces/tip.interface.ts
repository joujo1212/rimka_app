export default interface Tip {
  id: string;
  title?: string;
  content?: string;
  collapsed?: boolean;
  cats: string[]; // it's array of tag's 'slug''
}
