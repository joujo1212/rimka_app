import { Injectable } from '@angular/core';
import ApiService from './api.service';
import TagAsCategory from '../interfaces/tag-as-category.interface';
import { Tag } from '@tryghost/content-api';
import CacheService from './cache.service';

/**
 * Tags are used as categories for both recipes and tips
 */
@Injectable({providedIn: 'root'})
export default class TagService {
  private tags: TagAsCategory[];
  private CACHE_KEY_ITEM = 'tags';

  constructor(private api: ApiService, private cacheService: CacheService) {
  }

  public async getAllTags(): Promise<TagAsCategory[]> {
    try {
      const tags = await this.api.getApi().tags.browse({limit: 'all'});
      this.tags = tags.map(this.ghostTagToAppTag);
      await this.cacheService.setItem(this.CACHE_KEY_ITEM, this.tags);
      return this.tags;
    } catch (e) {
      console.warn('Fetching tags failed. Cache will be used. ', e);
      // if failed, load tags from cache
      const tags = await this.cacheService.getItem<TagAsCategory[]>(this.CACHE_KEY_ITEM) || [];
      this.tags = tags;
      return tags;
    }
  }

  public async getTipsFromTags(): Promise<TagAsCategory[]> {
    await this.getAllTags();
    // filter only tips and remove from name prefix 'Tip', if it has this prefix
    return this.tags
      .filter(t => t.id.startsWith('tip-'))
      .map(t => ({
        ...t,
        name: t.name.toLowerCase().replace('tip', '').trim()}));
  }

  private ghostTagToAppTag(tag: Tag): TagAsCategory {
    return {
      id: tag.slug,
      name: tag.name
    }
  }
}
