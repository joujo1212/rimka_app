import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appTouch]'
})
export class TouchDirective {

  constructor(private el: ElementRef) { }

  @HostListener('mousedown') onMouseEnter() {
    this.el.nativeElement.classList.add('touch');
    setTimeout(() => {
      this.el.nativeElement.classList.remove('touch');
    },350);
  }

}
