import { Component, OnInit } from '@angular/core';
import ProductService from '../../services/product.service';
import Package from '../../interfaces/package.interface';
import { animate, state, style, transition, trigger } from '@angular/animations';
import PopupsService from '../../services/popups.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-packages-page',
  templateUrl: './packages-page.component.html',
  styleUrls: ['./packages-page.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        overflow: 'hidden',
        height: '*',
        marginBottom: '18px'
      })),
      state('out', style({
        overflow: 'hidden',
        height: '150px',
        marginBottom: '0px'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ])
  ]
})
export class PackagesPageComponent implements OnInit {
  // we need to know what platform it is because of different Restore purchase mechanism
  public isIos = false;

  constructor(public productService: ProductService, private popupsService: PopupsService, private plt: Platform) { }

  ngOnInit() {
    if (this.plt.is('ipad') || this.plt.is('iphone') || this.plt.is('ios')) {
      this.isIos = true;
    }
  }

  public buy(product: Package) {
    this.productService.purchase(product);
  }

  public async restorePurchases() {
    this.productService.restore();
    await this.popupsService.showLoad('Obnovujem...', true, 2000);
    setTimeout(() => {
      this.popupsService.showAlert('Hotovo!', '', 'Ak balíčky stále nie sú obnovené správne, uistite sa, že máte zapnutý internet, vypnite a zapnite aplikáciu a akciu zopakujte');
    }, 2000);
  }
}
