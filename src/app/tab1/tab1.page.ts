import { Component, OnDestroy, OnInit } from '@angular/core';
import Recipe from '../interfaces/recipe.interface';
import { AlertController, IonRouterOutlet, ModalController, NavController } from '@ionic/angular';
import { ModalComponent } from '../shared/modal/modal.component';
import CategoryService from '../services/category.service';
import RecipeBookService, { RecipeBookEnum } from '../services/recipe-book.service';
import FavouriteService from '../services/favourite.service';
import Category from '../interfaces/category.interface';
import SearchService from '../services/search.service';
import NetworkService from '../services/network.service';
import { Subscription } from 'rxjs';
import ProductService from '../services/product.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit, OnDestroy{
  public filteredRecipes: Recipe[] = [];
  public searchText: string = '';
  public isLoading = false;
  public scrollDisabled = false;
  public recipes: Recipe[] = [];
  private recipeBookSub: Subscription;
  private readonly limit = 50;
  private offset = this.limit;
  private totalFilteredRecipes: number;
  private networkSub: Subscription;

  constructor(public modalController: ModalController, public categoryService: CategoryService,
              private routerOutlet: IonRouterOutlet, private recipeBookService: RecipeBookService,
              private navController: NavController, public favouriteService: FavouriteService,
              private searchService: SearchService, private alertController: AlertController,
              private networkService: NetworkService, private productService: ProductService) {}

  async ngOnInit() {
    // when recipe book was changed by user
    this.recipeBookSub = this.recipeBookService.selectedRecipeBook$.subscribe(recipeBook => {
      this.getRecipesByRecipeBook(recipeBook);
    });

    this.productService.ownedPackages.subscribe((ownedPackages: string[]) => {
      const recipeBook = this.recipeBookService.selectedRecipeBook;
      this.getRecipesByRecipeBook(recipeBook);
    });

    const isInternet = await this.networkService.isInternetAvailable();
    if (!isInternet.connected) {
      await this.noInternetAlert();
    }
    this.networkSub = this.networkService.networkChangeSub.subscribe(connected => {
      // if user has been connected to the internet, let's get new data
      if (connected) {
        setTimeout(() => {
          console.log('Actually owned packages: ', this.productService.ownedPackages.value);
          this.getRecipesByRecipeBook(this.recipeBookService.selectedRecipeBook);
        }, 2000);
      }
    })
  }

  ngOnDestroy() {
    if (this.networkSub) {
      this.networkSub.unsubscribe();
    }
  }

  private async getRecipesByRecipeBook(recipeBook: RecipeBookEnum) {
    this.isLoading = true;
    this.resetPagination();
    try {
      const allRecipes: Recipe[] = await this.recipeBookService.getAllRecipes();
      this.recipes = allRecipes.filter(r => r.recipeBook === recipeBook);
    } catch(e) {

    } finally {
      this.isLoading = false;
    }
    if (this.recipes) {
      this.filterRecipes();
    }
    this.isLoading = false;
  }

  async showCategoriesModal() {
    const modal = await this.modalController.create({
      component: ModalComponent,
      cssClass: 'custom-modal',
      mode: 'md',
      showBackdrop: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    await modal.present();
    const data  = await modal.onWillDismiss();
    const category: Category = data.data;
    if (category) {
      this.categoryService.selectedCategory = category;
      this.resetPagination();
      this.filterRecipes();
    }
  }

  private async noInternetAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Internet nie je dostupný',
      subHeader: '',
      message: 'Ak chcete mať dostupné najnovšie recepty a tipy, pripojte sa na internet a skúste to znova',
      buttons: ['OK']
    });

    await alert.present();
  }

  private filterRecipes() {
    const matchedRecipes = this.searchService.searchRecipes(this.categoryService.selectedCategory, this.searchText, this.recipes);
    this.totalFilteredRecipes = matchedRecipes.length;
    this.filteredRecipes = matchedRecipes.splice(0, this.offset);
  }

  public onSearch(text: string) {
    this.searchText = text;
    this.resetPagination();
    this.filterRecipes();
  }

  private resetPagination() {
    this.totalFilteredRecipes = 0;
    this.scrollDisabled = false;
    this.offset = this.limit;
  }

  public async doRefresh(event) {
    const recipeBook = this.recipeBookService.selectedRecipeBook;
    await this.getRecipesByRecipeBook(recipeBook);
    setTimeout(() => {
      event.target.complete();
    }, 500);
  }

  public getFavouriteRecipes() {
    const recipes = [];
    this.favouriteService.favouriteRecipes.forEach(id => {
      const recipe = this.recipes.find(r => r.id === id);
      // handle case when recipe stored in favourites is no longer available
      if (recipe) {
        recipes.push(recipe);
      }
    });
    return recipes;
  }

  public loadData(event) {
    setTimeout(() => {
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      this.offset += this.limit;
      this.filterRecipes();
      if (this.filteredRecipes.length === this.totalFilteredRecipes) {
        this.scrollDisabled = true;
      }
    }, 250);
  }
}
