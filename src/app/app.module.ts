import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {SharedModule} from './shared/shared.module';
import RecipeBookService from './services/recipe-book.service';
import ThemeService from './services/theme.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';
import * as SentryAngular from '@sentry/angular';
import { Offline as OfflineIntegration } from '@sentry/integrations';


SentryAngular.init(
  {
    debug:false,
    dsn: "https://ec63a3518bde404bb48e451cbffde1e2@o970640.ingest.sentry.io/5922069",

    // To set your release and dist versions
    release: "rimka-app@1.3.2",
    dist: "1",
    integrations: [new OfflineIntegration(
      {
        // limit how many events will be localled saved. Defaults to 30.
        maxStoredEvents: 30
      }
    )],
  }
);


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    SharedModule
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    RecipeBookService,
    ThemeService,
    InAppPurchase2,
    {
      provide: ErrorHandler,
      // Attach the Sentry ErrorHandler
      useValue: SentryAngular.createErrorHandler(),
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
