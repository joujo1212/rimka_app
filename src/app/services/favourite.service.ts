import { Injectable } from '@angular/core';
import CacheService from './cache.service';

@Injectable({providedIn: 'root'})
export default class FavouriteService {
  private _favouriteRecipes: string[] = [];
  private CACHE_ITEM_KEY = 'favourites';

  public get favouriteRecipes(): string[] {
    return this._favouriteRecipes;
  }

  constructor(private cacheService: CacheService) {
    this.loadFavourites();
  }

  public isRecipeFavourite(id: string) {
    return this._favouriteRecipes.some(i => i === id);
  }

  public removeFromFavourites(id: string) {
    this._favouriteRecipes = this._favouriteRecipes.filter(i => i !== id);
    this.storeFavourites();
  }

  public addToFavourites(recipeId: string) {
    this._favouriteRecipes.push(recipeId);
    this.storeFavourites();
  }

  private async storeFavourites() {
    await this.cacheService.setItem(this.CACHE_ITEM_KEY, this._favouriteRecipes);
  }

  private async loadFavourites() {
    const data = await this.cacheService.getItem<string[]>(this.CACHE_ITEM_KEY);
    this._favouriteRecipes = data || [];
  }
}
