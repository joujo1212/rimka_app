import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab1Page } from './tab1.page';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { PackagesPageComponent } from './packages-page/packages-page.component';

const routes: Routes = [
  {
    path: '',
    component: Tab1Page,
  },
  {
    path: 'detail/:id',
    component: RecipeDetailComponent,

  },
  {
    path: 'products',
    component: PackagesPageComponent,

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab1PageRoutingModule {}
