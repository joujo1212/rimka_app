import { Component } from '@angular/core';
import ShoppingListService from '../services/shopping-list.service';
import { AlertController } from '@ionic/angular';
import * as Sentry from '@sentry/angular';
import { Plugins } from '@capacitor/core';
const { Share } = Plugins;

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  public newItem: string;

  constructor(public shoppingListService: ShoppingListService, private alertController: AlertController) {}

  public toggleRadio(item) {
    this.shoppingListService.toggleItem(item);
  }

  public removeItem(index: number) {
    this.shoppingListService.removeItem(index);
  }

  public addItem() {
    if (!this.newItem) { return; }
    this.shoppingListService.addItem(this.newItem);
    this.newItem = '';
  }

  public async share() {
    const sharingText = 'Toto je môj nákupný zoznam z apky Rimka Recepty:\n' +
      this.shoppingListService.shoppingList.map(i => `- ${i.text}`).join('\n');

    try {
      let shareRet = await Share.share({
        title: 'Zdieľať nákupný zoznam',
        text: sharingText,
        dialogTitle: 'Zdieľať nákupný zoznam'
      });
    } catch (e) {
      Sentry.captureException('Sharing shopping list failed (not supported?): ', e);
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Problém so zdieľaním',
        subHeader: '',
        message: 'Na tomto zariadení nie je zdieľanie nákupného zoznamu podporované',
        buttons: ['OK']
      });

      await alert.present();
    }
  }
}
