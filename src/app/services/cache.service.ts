import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Injectable({providedIn: 'root'})
export default class CacheService {

  public async setItem(key: string, data: any): Promise<void> {
    await Storage.set({
      key,
      value: JSON.stringify(data)
    });
  }

  public async getItem<T>(key: string): Promise<T> {
    const data = await Storage.get({
      key
    });
    return JSON.parse(data.value);
  }

  public async clearAll() {
    await Storage.clear();
  }
}
