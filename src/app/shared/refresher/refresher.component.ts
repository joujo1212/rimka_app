import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-refresher',
  templateUrl: './refresher.component.html',
  styleUrls: ['./refresher.component.scss'],
})
export class RefresherComponent implements OnInit {
  @Output() onRefresh: EventEmitter<Event> = new EventEmitter<null>();
  constructor() { }

  ngOnInit() {}

  public handleRefresh(event: Event) {
    this.onRefresh.emit(event);
  }
}
