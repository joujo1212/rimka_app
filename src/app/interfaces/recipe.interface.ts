import Category from './category.interface';
import { RecipeBookEnum } from '../services/recipe-book.service';

export default interface Recipe {
  id: string;
  title: string;
  subtitle?: string;
  perex?: string;
  perexSecondLang?: string;
  mainImage?: string;
  images?: string[];
  preparationTime?: string; // already formatted time to e.g. hh:mm
  ingredients?: string[];
  procedure?: string[];
  procedureSecondLang?: string[];
  categories?: Category[];
  recipeBook?: RecipeBookEnum;
  fulltextIndex?: string;
  perexEnd?: string;
  portions: number | string; // string in case when we want to show '--' (portions not defined)
  formSize: string;
  ovenType: 'UP_DOWN' | 'UP_DOWN_VENT';
  owned?: boolean; // store info if recipe is part of owned recipe (calculated in app directly)
}
