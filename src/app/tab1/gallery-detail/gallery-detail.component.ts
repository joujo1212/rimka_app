import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-gallery-detail',
  templateUrl: './gallery-detail.component.html',
  styleUrls: ['./gallery-detail.component.scss'],
})
export class GalleryDetailComponent {
  @ViewChild('slides') slides;
  images: string[] = [];
  selectedImageIndex: number;
  constructor(private modalController: ModalController) { }

  ngOnChanges() {
  }
  public closeGallery() {
    this.modalController.dismiss(null);
  }

  slideDidLoad(event) {
    this.slides.update();
  }
}
