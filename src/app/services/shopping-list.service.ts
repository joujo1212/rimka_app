import { Injectable } from '@angular/core';
import ShoppingItem from '../interfaces/shopping-item.interface';
import CacheService from './cache.service';

@Injectable({providedIn: 'root'})
export default class ShoppingListService {
  public shoppingList: ShoppingItem[];
  private CACHE_ITEM_KEY = 'shoppingList';

  constructor(private cacheService: CacheService) {
    this.loadShoppingList();
  }

  public hasItemInShoppingList(text: string) {
    return this.shoppingList.some(i => i.text === text);
  }

  public addRemoveItem(text: string) {
    if (this.hasItemInShoppingList(text)) {
      this.removeItem(this.shoppingList.findIndex(i => i.text === text));
    } else {
      this.addItem(text);
    }
  }

  public addItem(text: string, checked = false) {
    if (!text) { return; }
    this.shoppingList.unshift({text: text.trim(), checked});
    this.storeShoppingList();
  }

  public removeItem(index: number) {
    this.shoppingList.splice(index, 1);
    this.storeShoppingList();
  }

  public toggleItem(item: ShoppingItem) {
    if (item) {
      item.checked = !item.checked;
      this.storeShoppingList();
    }
  }

  private async storeShoppingList() {
    await this.cacheService.setItem(this.CACHE_ITEM_KEY, this.shoppingList);
  }

  private async loadShoppingList() {
    this.shoppingList = await this.cacheService.getItem<ShoppingItem[]>(this.CACHE_ITEM_KEY) || [];
  }
}
