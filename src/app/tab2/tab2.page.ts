import { Component, OnDestroy, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import RecipeBookService, { RecipeBookEnum } from '../services/recipe-book.service';
import ThemeService from '../services/theme.service';
import { ActivatedRoute } from '@angular/router';
import Tip from '../interfaces/tip.interface';
import TipService from '../services/tip.service';
import TagService from '../services/tag.service';
import TagAsCategory from '../interfaces/tag-as-category.interface';
import { AlertController } from '@ionic/angular';
import NetworkService from '../services/network.service';
import ProductService from '../services/product.service';
import { Subscription } from 'rxjs';
import * as Sentry from '@sentry/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        overflow: 'hidden',
        height: '*',
        marginBottom: '18px'
      })),
      state('out', style({
        opacity: '0',
        overflow: 'hidden',
        height: '0px',
        marginBottom: '0px'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ])
  ]
})
export class Tab2Page implements OnInit, OnDestroy {
  public categories: TagAsCategory[] = [];
  public tips: Tip[] = [];
  public filteredTips: Tip[] = [];
  private ALL_ID = '';
  public selectedCatId = this.ALL_ID;
  public isLoading = false;
  public isLocked =  true;
  private packagesSub: Subscription;

  constructor(private recipeBookService: RecipeBookService, private themeService: ThemeService,
              private activatedRoute: ActivatedRoute, private tipService: TipService,
              private tagService: TagService, private alertController: AlertController,
              private networkService: NetworkService, private productService: ProductService) {
  }

  async ngOnInit() {
    // always set RIMKA recipe book when goes to this page
    this.activatedRoute.url.subscribe(() => {
      this.recipeBookService.selectedRecipeBook = RecipeBookEnum.RIMKA;
      this.themeService.setRimkaTheme();
    });
    this.productService.ownedPackages.subscribe(async packages => {
      if (packages && packages.length > 0) {
        await this.loadData();
        this.isLocked = false;
      } else {
        this.isLocked = true;
      }
    })
  }

  ngOnDestroy() {
    if (this.packagesSub) {
      this.packagesSub.unsubscribe();
    }
  }

  private async loadData() {
    this.isLoading = true;
    this.tips = await this.tipService.getAllTips();
    this.categories = await this.tagService.getTipsFromTags();
    this.categories.unshift({id: this.ALL_ID, name: 'Všetko'});
    this.filterTips(this.selectedCatId);
    if (this.tips && this.tips.length === 0) {
      this.noTipsAlert();
      const sub = this.networkService.networkChangeSub.subscribe(connected => {
        if (connected) {
          setTimeout(() => {
            this.ngOnInit();
            sub.unsubscribe();
          }, 2000);
        }
      })
    }
    this.isLoading = false;
  }

  public filterTips(id: string) {
    this.selectedCatId = id;
    // filter by cat ID or if id === ALL_ID, show all tips
    this.filteredTips = this.tips.filter(t => t.cats.includes(id) || id === this.ALL_ID)
  }

  private async noTipsAlert() {
    Sentry.captureException('No tips alert');
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Žiadne Tipy a Rady',
      subHeader: 'Internet nie je dostupný',
      message: 'V smartfóne neboli nájdené žiadne tipy a rady. Pripojete sa na internet a otvorte aplikáciu znova',
      buttons: ['OK']
    });

    await alert.present();
  }

  public async doRefresh(event) {
    await this.loadData();
    setTimeout(() => {
      event.target.complete();
    }, 1000);
  }
}
