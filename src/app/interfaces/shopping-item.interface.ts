export default interface ShoppingItem {
  checked: boolean;
  text: string;
}
