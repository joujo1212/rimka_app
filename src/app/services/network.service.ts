import { Injectable, NgZone } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { NetworkStatus } from '@capacitor/core/dist/esm/core-plugin-definitions';
const { Network } = Plugins;

@Injectable({providedIn: 'root'})
export default class NetworkService {
  private networkStatusChange: Subject<boolean> = new Subject<boolean>();
  public networkChangeSub: Observable<Boolean> = this.networkStatusChange.asObservable();

  constructor(private zone: NgZone) {
    Network.addListener('networkStatusChange', (status) => {
      this.zone.runTask(() => {
        this.networkStatusChange.next(status.connected);
      })
    });
  }

  public isInternetAvailable(): Promise<NetworkStatus> {
    return Network.getStatus()
  }
}
