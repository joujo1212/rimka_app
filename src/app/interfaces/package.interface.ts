import { IAPProduct } from '@ionic-native/in-app-purchase-2';

export default interface Package extends IAPProduct {
  recipesCount: number;
  recipesList: string[];
}
