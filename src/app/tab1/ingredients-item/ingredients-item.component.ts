import { Component, Input, OnInit } from '@angular/core';
import ShoppingListService from '../../services/shopping-list.service';
import { ViewEncapsulation } from '@angular/core'
import PopupsService from '../../services/popups.service';

@Component({
  selector: 'app-ingredients-item',
  templateUrl: './ingredients-item.component.html',
  styleUrls: ['./ingredients-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IngredientsItemComponent implements OnInit {
  @Input() item: string;

  constructor(public shoppingListService: ShoppingListService, public popupsService: PopupsService) { }

  ngOnInit() {}

  public addToShoppingList(item: string) {
    this.shoppingListService.addRemoveItem(item);
    if (this.shoppingListService.hasItemInShoppingList(item)) {
      this.popupsService.showToast('Ingrediencia bola pridaná do Nákupného zoznamu');
    } else {
      this.popupsService.showToast('Ingrediencia bola odstránená z Nákupného zoznamu');
    }
  }
}
