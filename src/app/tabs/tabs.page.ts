import { Component } from '@angular/core';
import ThemeService from '../services/theme.service';

interface Tab {
  icon: string;
  label: string;
}
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  public tabs: Tab[] = [
    {label: 'Recepty', icon: '/assets/icon/icon-recipes.svg'},
    {label: 'Tipy', icon: '/assets/icon/icon-tips.svg'},
    {label: 'N. zoznam', icon: '/assets/icon/icon-list.svg'},
    {label: 'O mne', icon: '/assets/icon/icon-about.svg'},
    {label: 'Kontakt', icon: '/assets/icon/icon-contact.svg'}
  ]
  constructor(public themeService: ThemeService) {}

}
