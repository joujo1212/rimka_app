import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Recipe from '../../interfaces/recipe.interface';
import RecipeBookService, { RecipeBookEnum } from '../../services/recipe-book.service';
import FavouriteService from '../../services/favourite.service';
import ShoppingListService from '../../services/shopping-list.service';
import PopupsService from '../../services/popups.service';
import PdfService from '../../services/pdf.service';
import * as Sentry from '@sentry/angular';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.scss'],
})
export class RecipeDetailComponent implements OnInit {
  public recipe: Recipe;
  public RecipeBookEnum = RecipeBookEnum;
  public isInFavourite = false;
  public isSecondLangActive = false;
  private id: string;

  constructor(public recipeBookService: RecipeBookService, private activatedRoute: ActivatedRoute,
              private favouriteService: FavouriteService,
              public shoppingListService: ShoppingListService, private popupsService: PopupsService,
              private pdfService: PdfService, private router: Router) { }

  async ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.recipe = await this.recipeBookService.getRecipe(this.id);
    if (this.recipe && !this.recipe.owned) {
      await this.router.navigate(['/']);
    }
    if (!this.recipe) { return; }
    this.isInFavourite = this.favouriteService.isRecipeFavourite(this.recipe.id);
  }

  public async share() {
    Sentry.captureMessage('Share button fired (PDF export)');
    const loader = await this.popupsService.showLoad('Generujem PDF súbor', true);
    await this.pdfService.generatePdf(this.recipe);
    await loader.dismiss();
  }

  public toggleInFavourites() {
    if (this.isInFavourite) {
      this.favouriteService.removeFromFavourites(this.recipe.id);
      this.popupsService.showToast('Recept bol odstránený z obľúbených');
    } else {
      this.favouriteService.addToFavourites(this.recipe.id);
      this.popupsService.showToast('Recept bol pridaný medzi obľúbené');
    }
    this.isInFavourite = !this.isInFavourite;
  }

  public languageChanged(event) {
    this.isSecondLangActive = event.detail.checked;
  }


}
