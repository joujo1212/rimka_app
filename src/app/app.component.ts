import { ChangeDetectorRef, Component } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
import { IAPProduct } from '@ionic-native/in-app-purchase-2';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor() {

  }

}
