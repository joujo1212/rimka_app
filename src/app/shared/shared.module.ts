import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import {HeaderComponent} from './header/header.component';
import { ModalComponent } from './modal/modal.component';
import { BuyPackageComponent } from './buy-package/buy-package.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { RefresherComponent } from './refresher/refresher.component';
import { TouchDirective } from './touch.directive';
import { NoImageDirective } from './no-image.directive';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  declarations: [
    HeaderComponent,
    ModalComponent,
    BuyPackageComponent,
    RefresherComponent,
    TouchDirective,
    NoImageDirective,
    LoaderComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    ModalComponent,
    BuyPackageComponent,
    RefresherComponent,
    TouchDirective,
    NoImageDirective,
    LoaderComponent
  ],
  providers: [
    DatePipe
  ]
})
export class SharedModule { }
