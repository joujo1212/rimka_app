import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter, Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, tap } from 'rxjs/operators';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit, AfterViewInit, OnDestroy {
  @Output() changeText: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('input') input: ElementRef;
  private sub: Subscription;

  constructor() { }

  ngOnInit() {}

  ngAfterViewInit() {
    this.sub = fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        filter(Boolean),
        debounceTime(150),
        distinctUntilChanged(),
        tap(text => {
          this.changeText.emit(this.input.nativeElement.value);
        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    if(this.sub) {
      this.sub.unsubscribe();
    }
  }

  public clearInput() {
    this.input.nativeElement.value = '';
    this.changeText.emit('');
  }
}
