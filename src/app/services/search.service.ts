import Category from '../interfaces/category.interface';
import Recipe from '../interfaces/recipe.interface';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export default class SearchService {

  public searchRecipes(category: Category, searchText: string, recipes: Recipe[]) {
    let filteredRecipes: Recipe[];
    // filter by category
    if (!category) {
      // show all recipes
      filteredRecipes = [...recipes];
    } else {
      filteredRecipes = recipes.filter(r => r.categories.some(c => c.id === category.id));
    }

    // filter by fulltext search
    return filteredRecipes.filter(r => (
      r.fulltextIndex.includes(SearchService.normalizeText(searchText))
    ));
  }

  /**
   * Removes diacritic, to lower case, trim all spaces (inside of text also)
   * @param text
   */
  public static normalizeText(text: string) {
    return text
      .toLowerCase()
      .trim()
      .normalize('NFD')
      .replace(/[\u0300-\u036f\s]/g, '');
  }
}
