import { Injectable } from '@angular/core';
import ApiService from './api.service';
import { PostOrPage } from '@tryghost/content-api';
import Recipe from '../interfaces/recipe.interface';
import Category from '../interfaces/category.interface';
import CategoryService from './category.service';
import SearchService from './search.service';
import CacheService from './cache.service';
import { AlertController, LoadingController } from '@ionic/angular';
import PopupsService from './popups.service';
import { Subject } from 'rxjs';
import { DatePipe } from '@angular/common';
import ProductService from './product.service';
import * as Sentry from '@sentry/angular';

export enum RecipeBookEnum {
  RIMKA,
  CHLOP
}
@Injectable()
export default class RecipeBookService {
  private recipes: Recipe[];
  private _selectedRecipeBook: RecipeBookEnum = RecipeBookEnum.RIMKA;
  private CACHE_KEY_ITEM = 'recipes';
  private selectedRecipeBookSub: Subject<RecipeBookEnum> = new Subject();
  public selectedRecipeBook$ = this.selectedRecipeBookSub.asObservable();
  public set selectedRecipeBook(recipeBook: RecipeBookEnum) {
    // if it's setting to the same recipe book, do nothing
    if (this._selectedRecipeBook === recipeBook) { return; }
    this._selectedRecipeBook = recipeBook;
    this.selectedRecipeBookSub.next(recipeBook);
  }

  public get selectedRecipeBook() {
    return this._selectedRecipeBook;
  }

  constructor(private api: ApiService, private categoryService: CategoryService, private cacheService: CacheService,
              private loadingController: LoadingController, private alertController: AlertController,
              private popupsService: PopupsService, private datePipe: DatePipe, private productService: ProductService) {
  }

  /**
   * Get all recipes for rimka and chlop. It will be stored in cache together
   */
  public async getAllRecipes(): Promise<Recipe[]> {
    try {
      let filter = `featured:true`;
      const ownedPackaged = this.productService.ownedPackages.value;
      if (ownedPackaged && ownedPackaged.length > 0) {
        filter += `, tag:[${ownedPackaged.join(',')}]`;
      }
      // do not use filter for now
      filter = '';
      const order = 'featured DESC, published_at DESC, created_at DESC';

      const posts = await this.api.getApi().posts.browse({order, filter, include: ['tags', 'authors'], limit: 'all'});
      this.recipes = posts.map(this.postToRecipe.bind(this));
      // store recipes asynchronously
      this.cacheAllRecipes(this.recipes);
      return this.recipes;
    } catch (e) {
      console.warn('Fetching recipes failed, cached data will be used. ', e);
      Sentry.captureException('Fetching recipes failed, cached data will be used.', e);
      const recipes: Recipe[] = await this.cacheService.getItem<Recipe[]>(this.CACHE_KEY_ITEM);
      this.recipes = recipes;
      return recipes;
    }
  }

  private async cacheAllRecipes(recipes: Recipe[]): Promise<any> {
    // For now we are caching only recipes, NOT images.
    // const loader = await this.popupsService.showToast('Ukladám recepty do zariadenia...');
    await this.cacheService.setItem(this.CACHE_KEY_ITEM, recipes);
    // const imagesPromises = new Set();
    // recipes.forEach(r => {
    //   imagesPromises.add(this.preloadImage(r.mainImage));
    //   r.images.forEach(i => {
    //     imagesPromises.add(this.preloadImage(i));
    //   })
    // })
    // return Promise.all(imagesPromises).then(() => {
    //   console.log('Obrazky nacitane!');
    //   loader.dismiss();
    //   this.popupsService.showToast('Všetky recepty sú od teraz dostupné aj offline', 2000);
    // });
  }

  public async getRecipe(id: string): Promise<Recipe> {
    // let's try fetch recipe
    try {
      let recipe = await this.fetchRecipe(id);
      return recipe;
    } catch(e) {
      console.warn('Fetching one recipe failed, cached will be used. ', e);
      Sentry.captureException('Fetching one recipe failed, cached data will be used.', e);
      // if fetching recipe failed, let's have a look to recipes list in service

      let recipe = this.recipes && this.recipes.find(i => i.id === id);
      // if it isn't in list, let's load cache and then have a look to list again
      if (!recipe) {
        this.recipes = await this.cacheService.getItem<Recipe[]>(this.CACHE_KEY_ITEM) || [];
        recipe = this.recipes.find(i => i.id === id);
      }
      return recipe;
    }
  }

  public getCategories(): Category[] {
    switch (this.selectedRecipeBook) {
      case RecipeBookEnum.RIMKA:
        return this.categoryService.rimkaCategories;
      case RecipeBookEnum.CHLOP:
        return this.categoryService.chlopCategories;
      default:
        return [];
    }
  }

  public parseCategoriesIntoText(categories: Category[]) {
    if (!categories) { return 'Ostatné'; }
    const names: string[] = categories.map(c => c.title + ' ' + (c.subtitle || ''));
    return names.join(', ');
  }

  private async fetchRecipe(id: string): Promise<Recipe> {
    const post = await this.api.getApi().posts.read({id}, {include: ['tags', 'authors']});
    return this.postToRecipe(post);
  }

  private postToRecipe(post: PostOrPage): Recipe {
    const ownedPackaged = this.productService.ownedPackages.value;

    const html = post.html || '';
    const imagesRegex = RegExp('<img src="(.*?)"','gi');
    const subtitleRegex = RegExp('<h2(.*?)>(.*?)</h2>','i');
    const perexRegex = RegExp('<blockquote>(.*?)</blockquote>','ig');
    const perexEndRegex = RegExp('<em>(.*?)</em>','gis');
    const ingredientsRegex = RegExp('<ul>(.*?)</ul>','is');
    const ingredientsItemRegex = RegExp('<li>(.*?)</li>','gis');
    const procedureRegex = RegExp('<ol>(.*?)</ol>','gis');
    const procedureItemRegex = RegExp('<li>(.*?)</li>','gis');
    const portionsRegex = RegExp('<h3(.*?)>(.*?)</h3>','gis');
    const formSizeRegex = RegExp('<h4(.*?)>(.*?)</h4>','gi');

    const id = post.id;
    const title = post.title;
    const subtitleMatches = html.match(subtitleRegex);
    const subtitle = subtitleMatches && subtitleMatches[2] || '';
    const perexMatches = this.matchAll(perexRegex, html);
    const perex = perexMatches && perexMatches[0] || '';
    const perexSecondLang = perexMatches && perexMatches[1] || '';
    const perexEndMatches = this.matchAll(perexEndRegex, html);
    const perexEnd = perexEndMatches && perexEndMatches.join('\n\n') || '';
    const images = this.matchAll(imagesRegex, html);
    const portions = parseInt(this.matchAll(portionsRegex, html, 2)[0]) || '--';
    const formSize = this.matchAll(formSizeRegex, html, 2)[0] || '--';

    let mainImage = post.feature_image || images[0];
    // inject image resolution into the path
    mainImage = mainImage.replace('content/images', 'content/images/size/w300');

    const date = new Date(post.published_at);
    const preparationTime = (date.getUTCHours() + '').padStart(2, '0') + ':' + (date.getUTCMinutes() + '').padStart(2, '0');

    const categories: Category[] = [];
    let owned: boolean = post.featured; // if recipe is featured, it's owned automatically (as it's free recipe)
    if (post && post.tags && post.tags) {
      const existingCategories = this.getCategories();
      post.tags.forEach(p => {
        const foundCat = existingCategories.find(i => i.id === p.slug);
        if (foundCat) {
          categories.push(foundCat);
        }

        // check if recipe is owned by already owned packages
        if (!owned && ownedPackaged.includes(p.slug)) {
          owned = true;
        }
      });
    }

    const ingredientsList = html.match(ingredientsRegex) || [];
    const ingredients = this.matchAll(ingredientsItemRegex, ingredientsList[1]);

    const procedureList = this.matchAll(procedureRegex, html);
    const procedure = this.matchAll(procedureItemRegex, procedureList[0]);
    const procedureSecondLang = this.matchAll(procedureItemRegex, procedureList[1]);

    let ovenType;
    if (post.tags.some(t => t.slug === 'rura-hore-dole')) {
      ovenType = 'UP_DOWN';
    }
    if (post.tags.some(t => t.slug === 'rura-hore-dole-s-ventilaciou')) {
      ovenType = 'UP_DOWN_VENT';
    }

    const recipeBook = post.authors.find(a => a.slug === 'rimka') ? RecipeBookEnum.RIMKA : RecipeBookEnum.CHLOP;
    const recipe: Recipe = {
      id,
      title,
      subtitle,
      perex,
      perexSecondLang,
      mainImage,
      images,
      preparationTime,
      ingredients,
      procedure,
      procedureSecondLang,
      categories,
      perexEnd,
      recipeBook,
      portions,
      formSize,
      ovenType,
      owned
    };
    recipe.fulltextIndex = this.fulltextIndexing(recipe, ['title', 'subtitle']);

    return recipe;
  }

  private preloadImage(src) {
    return new Promise(r => {
      const image = new Image()
      image.onload = r
      image.onerror = r
      image.src = src
    });
  }

  private matchAll(regex: RegExp, text: string, matchIndex = 1): string[] {
    let match;
    const result = [];
    let counter = 0;
    while ((match = regex.exec(text)) !== null) {
      result.push(match[matchIndex]);
      counter++;
      // disallow to freeze page when wrong regex is written and while cycle is running infinitely
      if (counter === 1000) {
        Sentry.captureException('Recipe Book Service:MatchAll counter reached');
        break;
      }
    }
    return result;
  }

  /**
   * Prepare text for fulltext searching. It can be stored to any object (recipe e.g.) so searching
   * will be fast with pre-indexed object
   * @param recipe
   * @param fieldsForFulltext
   */
  private fulltextIndexing(recipe: Recipe, fieldsForFulltext: string[]) {
    let fulltextIndex = '';
    fieldsForFulltext.forEach(f => {
      if (recipe[f]) {
        fulltextIndex += SearchService.normalizeText(recipe[f]);
      }
    });
    return fulltextIndex;
  }

}
