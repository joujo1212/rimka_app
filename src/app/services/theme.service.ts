import { chlopTheme, rimkaTheme, Theme } from '../interfaces/theme.interface';

export default class ThemeService {
  private active: Theme;
  private availableThemes: Theme[] = [rimkaTheme, chlopTheme];

  constructor() {
    this.setActiveTheme(rimkaTheme);
  }

  public setRimkaTheme(): void {
    this.setActiveTheme(rimkaTheme);
  }
  public setChopTheme(): void {
    this.setActiveTheme(chlopTheme);
  }

  private setActiveTheme(theme: Theme): void {
    this.active = theme;
    Object.keys(this.active.properties).forEach(property => {
      document.documentElement.style.setProperty(
        property,
        this.active.properties[property]
      );
    });
  }
}
