import { Component, Input, OnInit } from '@angular/core';
import RecipeBookService, { RecipeBookEnum } from '../../services/recipe-book.service';
import ThemeService from '../../services/theme.service';
import CategoryService from '../../services/category.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() showBackButton = false;
  @Input() title: string; // if title is set, app logo is hidden
  @Input() secondaryColor = false;
  // if true, clicking on any logo (active or minor) will cause switch recipe book
  @Input() enableToggleRecipeBook = false;

  public RecipeBookEnum = RecipeBookEnum;
  constructor(public recipeBookService: RecipeBookService, public categoryService: CategoryService,
              private themeService: ThemeService, private navController: NavController) { }

  ngOnInit() {
  }

  private selectRimkaRecipeBook() {
    this.recipeBookService.selectedRecipeBook = RecipeBookEnum.RIMKA;
    this.themeService.setRimkaTheme();
    // reset category when changing recipe book
    this.categoryService.selectedCategory = null;
  }

  private selectChlopRecipeBook() {
    this.recipeBookService.selectedRecipeBook = RecipeBookEnum.CHLOP;
    this.themeService.setChopTheme();
    // reset category when changing recipe book
    this.categoryService.selectedCategory = null;
  }

  public toggleRecipeBook() {
    if (!this.enableToggleRecipeBook) { return; }

    if (this.recipeBookService.selectedRecipeBook === RecipeBookEnum.RIMKA) {
      this.selectChlopRecipeBook();
    } else {
      this.selectRimkaRecipeBook();
    }
  }

  public goBack() {
    this.showBackButton = false;
    this.navController.navigateBack('/tabs/tab1');
  }
}
