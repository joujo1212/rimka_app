import { Component, Input, OnInit } from '@angular/core';
import ThemeService from '../../services/theme.service';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { GalleryDetailComponent } from '../gallery-detail/gallery-detail.component';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent implements OnInit {
  @Input() images: string[] = [];
  public selectedImageIndex: number;
  constructor(private themeService: ThemeService, private modalController: ModalController, private routerOutlet: IonRouterOutlet) { }

  ngOnInit() {
  }

  public openGallery(index: number) {
    this.selectedImageIndex = index;
    this.showGalleryDetail();
  }

  async showGalleryDetail() {
    const modal = await this.modalController.create({
      component: GalleryDetailComponent,
      cssClass: 'gallery-detail-modal',
      mode: 'md',
      showBackdrop: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: {
        images: this.images,
        selectedImageIndex: this.selectedImageIndex
      }
    });
    await modal.present();
  }
}
