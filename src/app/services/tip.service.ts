import { Injectable } from '@angular/core';
import ApiService from './api.service';
import { PostOrPage } from '@tryghost/content-api';
import Tip from '../interfaces/tip.interface';
import CacheService from './cache.service';

@Injectable({providedIn: 'root'})
export default class TipService {
  private tips: Tip[] = [];
  private CACHE_ITEM_KEY = 'tips';

  constructor(private api: ApiService, private cacheService: CacheService) {
  }

  public async getAllTips(): Promise<Tip[]> {
    try {
      const pages = await this.api.getApi().pages.browse({include: ['tags'], limit: 'all'});
      this.tips = pages.map(this.pageToTip);
      await this.cacheService.setItem(this.CACHE_ITEM_KEY, this.tips);
      return this.tips;
    } catch(e) {
      console.warn('Fetching tips failed. Cache will be used. ', e);
      // if failed, load tips from cache
      const tips = await this.cacheService.getItem<Tip[]>(this.CACHE_ITEM_KEY) || [];
      this.tips = tips;
      return tips;
    }
  }

  private pageToTip(page: PostOrPage): Tip {
    return {
      id: page.id,
      title: page.title,
      content: page.html,
      collapsed: true,
      cats: page.tags && page.tags.map(t => t.slug) || []
    };
  }
}
