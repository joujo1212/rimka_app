import { Injectable } from '@angular/core';
import Recipe from '../interfaces/recipe.interface';
import { jsPDF } from "jspdf";
import { Plugins, FilesystemDirectory } from '@capacitor/core';
import Category from '../interfaces/category.interface';
import RecipeBookService from './recipe-book.service';

const {Share, Filesystem} = Plugins;

const PDF_MARGINS = 25; // in px

@Injectable({providedIn: 'root'})
export default class PdfService {
  private fontPath = '/assets/fonts/baloo_chettan_2/BalooChettan2-Regular.ttf';

  constructor(private recipeBookService: RecipeBookService) {
  }

  public async generatePdf(r: Partial<Recipe>) {
    // Default export is a4 paper, portrait, using millimeters for units
    const doc = new jsPDF('p', 'px', 'a4');

    // TODO fix loading images in Safari
    // for (let i = 0; i < r.images.length; i++) {
    //   try {
    //     await this.fetchAndAddImageToPdf(r.images[i], doc, i);
    //   } catch (e) {
    //     console.warn(e);
    //   }
    // }

    doc.setFontSize(10);
    doc.addFont(this.fontPath, 'baloo', 'normal');
    doc.setFont('baloo');
    await doc.html(`<body>
<h1 style="width: 400px; font-family: baloo; font-size: 18px; color: #000;">${r.title}</h1>
<h2 style="width: 400px; font-family: baloo; font-size: 14px; color: #000;">${r.subtitle}</h2>
<p style="width: 400px; font-family: baloo; font-size: 10px; color: #000;">Kategória: ${this.recipeBookService.parseCategoriesIntoText(r.categories)}</p>
<p style="width: 400px; font-family: baloo; font-size: 10px; color: #000;">Čas prípravy: ${r.preparationTime}</p>
<p style="width: 400px; font-family: baloo; font-size: 10px; color: #000;">${r.perex}</p>
<h3 style="width: 400px; font-family: baloo; font-size: 10px; color: #000;">Ingrediencie</h3>
<ul style="width: 400px; font-family: baloo; font-size: 10px; color: #000;">
${r.ingredients.map(i => '<li style="list-style-type: none">- ' + i + '</li>').join('')}
</ul>
<h3 style="width: 400px; font-family: baloo; font-size: 10px; color: #000;">Postup</h3>
<ol style="width: 400px; font-family: baloo; font-size: 10px; color: #000;">
${r.procedure.map((i, index) => `<li style="list-style-type: none">${index + 1}. ${i}</li>`).join('')}
</ol>
<p style="width: 400px; font-family: baloo; font-size: 10px; color: #000;">${r.perexEnd}</p>
<!--<img width="100px" height="100px" src="${r.mainImage}">-->
</body>
`, {x: PDF_MARGINS, y: 0 + PDF_MARGINS});

    const filename = `Recept od Rimky - ${r.title}.pdf`;
    const data = await doc.output("dataurlstring", {filename});
    const file = await Filesystem.writeFile({
      data: data,
      path: filename,
      directory: FilesystemDirectory.Cache
    });
    try {
      let shareRet = await Share.share({
        title: 'Zdieľať tento recept',
        text: 'Zdieľajte recept alebo si ho kľudne vytlačte na papier',
        url: file.uri,
        dialogTitle: 'Zdieľať tento recept'
      });
    } catch (e) {
      await doc.save(filename);
    }
  }

  private async fetchAndAddImageToPdf(imageUrl: string, doc: jsPDF, index: number): Promise<string> {
    const headers = {
      'Access-Control-Allow-Headers': 'Content-Type, origin',
      'Access-Control-Allow-Origin': 'http://localhost:4200',
      'mode': 'no-cors'
    }
    const img = await fetch('http://blog.invelogy.sk/content/images/2021/03/IMG_3521.JPG');
    const blob = await img.blob();
    // const blob = await this.httpClient.get('http://blog.invelogy.sk/content/images/2021/03/IMG_3521.JPG', { responseType: 'blob', headers }).toPromise();
    const reader = new FileReader();
    return new Promise((resolve, reject) => {
      reader.onload = function () {
        doc.addImage({
          // @ts-ignore
          imageData: this.result,
          x: PDF_MARGINS + index * (100 + PDF_MARGINS), y: PDF_MARGINS, width: 100, height: 100
        });
        resolve('');
      };
      reader.readAsDataURL(blob);
    });
  }
}
