import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';

import { Tab1PageRoutingModule } from './tab1-routing.module';
import { SharedModule } from '../shared/shared.module';
import { RecipeItemComponent } from '../recipe-item/recipe-item.component';
import { SearchBarComponent } from '../search-bar/search-bar.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { GalleryComponent } from './gallery/gallery.component';
import { GalleryDetailComponent } from './gallery-detail/gallery-detail.component';
import { IngredientsItemComponent } from './ingredients-item/ingredients-item.component';
import { PackagesPageComponent } from './packages-page/packages-page.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    Tab1PageRoutingModule,
    SharedModule
  ],
    declarations: [
      Tab1Page,
      RecipeItemComponent,
      SearchBarComponent,
      RecipeDetailComponent,
      GalleryComponent,
      GalleryDetailComponent,
      IngredientsItemComponent,
      PackagesPageComponent
    ]
})
export class Tab1PageModule {}
