import { Component, OnInit } from '@angular/core';
import Category from '../../interfaces/category.interface';
import { ModalController } from '@ionic/angular';
import RecipeBookService from '../../services/recipe-book.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  constructor(private modalCtrl: ModalController, public recipeBookService: RecipeBookService) { }

  ngOnInit() {
  }

  public selectCategory(category: Category) {
    this.modalCtrl.dismiss(category);
  }

  public close() {
    this.modalCtrl.dismiss(null);
  }

}
